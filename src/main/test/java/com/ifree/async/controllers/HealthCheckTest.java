package com.ifree.async.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class HealthCheckTest {

    @InjectMocks
    private HealthCheckController healthCheckController;

    @Test
    public void checkHealth() {
        String response = healthCheckController.checkHealth();
        assertEquals("response", "I'm ok!", response);
    }

}
