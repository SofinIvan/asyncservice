package com.ifree.async.controllers;

import com.ifree.async.enums.ApplicationStatus;
import com.ifree.async.exceptions.ApplicationNotFoundException;
import com.ifree.async.exceptions.ApplicationNotReadyException;
import com.ifree.async.model.ApplicationA;
import com.ifree.async.model.dto.ApplicationDTO;
import com.ifree.async.service.ApplicationService;
import com.ifree.async.utils.ApplicationConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.ifree.async.utils.TestConstants.APPLICATION_CONTROLLER_REQUEST_URL;
import static com.ifree.async.utils.TestConstants.EMPTY_STRING;
import static com.ifree.async.utils.TestConstants.ID;
import static com.ifree.async.utils.TestConstants.JSON_INPUT;
import static com.ifree.async.utils.TestConstants.MEDIA_TYPE;
import static com.ifree.async.utils.TestEntityCreator.createApplicationAEntity;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:spring/dispatcher-servlet.xml")
public class ApplicationControllerAdviceTest {

    private ApplicationA applicationA;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ApplicationConverter applicationConverter;

    @Autowired
    private ApplicationService applicationService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        applicationA = createApplicationAEntity();
    }

    @Test
    public void testApplicationControllerAdviceWithAnyException() throws Exception {
        given(applicationConverter.fromDTO(Mockito.any(ApplicationDTO.class)))
                .willReturn(applicationA);

        ClassCastException classCastException = new ClassCastException("ClassCastException occurred during tests");
        when(applicationService.save(applicationA))
                .thenThrow(classCastException);

        String expectedErrorMessage = "Exception with type: 'ClassCastException', message: 'ClassCastException occurred during tests'";
        mockMvc.perform(post(APPLICATION_CONTROLLER_REQUEST_URL)
                .accept(MEDIA_TYPE)
                .content(JSON_INPUT)
                .contentType(MEDIA_TYPE))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason(expectedErrorMessage))
                .andExpect(content().string(EMPTY_STRING));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void testApplicationControllerAdviceWithApplicationNotFoundException() throws Exception {
        String exceptionMessage = String.format("Application with id '%s' not found", ID);
        when(applicationService.getApplication(ID))
                .thenThrow(new ApplicationNotFoundException(exceptionMessage));

        String expectedErrorMessage = "Exception with type: 'ApplicationNotFoundException', message: 'Application with id '" + ID + "' not found'";
        mockMvc.perform(get(APPLICATION_CONTROLLER_REQUEST_URL + "/" + ID))
                .andExpect(status().isNotFound())
                .andExpect(status().reason(expectedErrorMessage))
                .andExpect(content().string(EMPTY_STRING));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void testApplicationControllerAdviceWithApplicationNotReadyException() throws Exception {
        String exceptionMessage = String.format("Application with id '%s' not ready, status is: '%s'", ID, ApplicationStatus.IN_PROCESS.getCode());
        when(applicationService.getApplication(ID))
                .thenThrow(new ApplicationNotReadyException(exceptionMessage));

        String expectedErrorMessage = String.format("Exception with type: 'ApplicationNotReadyException', message: 'Application with id '%s' not ready, status is: '%s''", ID, ApplicationStatus.IN_PROCESS.getCode());
        mockMvc.perform(get(APPLICATION_CONTROLLER_REQUEST_URL + "/" + ID))
                .andExpect(status().isLocked())
                .andExpect(status().reason(expectedErrorMessage))
                .andExpect(content().string(EMPTY_STRING));
    }


}
