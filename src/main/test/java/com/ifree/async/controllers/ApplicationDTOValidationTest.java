package com.ifree.async.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ifree.async.model.Application;
import com.ifree.async.model.ApplicationA;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.ifree.async.utils.TestConstants.APPLICATION_CONTROLLER_REQUEST_URL;
import static com.ifree.async.utils.TestConstants.EMPTY_STRING;
import static com.ifree.async.utils.TestConstants.MEDIA_TYPE;
import static com.ifree.async.utils.TestEntityCreator.createApplicationAEntity;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:spring/dispatcher-servlet.xml")
public class ApplicationDTOValidationTest {

    private static final String EXCEPTION_NOT_VALID_EXCEPTION_MESSAGE = "Exception with type: 'MethodArgumentNotValidException'";

    private static ObjectMapper mapper;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    private Application application;

    @BeforeClass
    public static void init() {
        mapper = new ObjectMapper();
    }

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void testShortDescriptionValidation() throws Exception {
        application = createApplicationAEntity();
        application.setDescription("d");

        MvcResult result = mockMvc.perform(post(APPLICATION_CONTROLLER_REQUEST_URL)
                .accept(MEDIA_TYPE)
                .content(createJsonString(application))
                .contentType(MEDIA_TYPE))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(EMPTY_STRING))
                .andReturn();

        String contentAsString = result.getResponse().getErrorMessage();
        assertTrue(contentAsString.contains(EXCEPTION_NOT_VALID_EXCEPTION_MESSAGE));
        assertTrue(contentAsString.contains("Description should have at least 3 characters"));
    }

    @Test
    public void testShortInfoValidation() throws Exception {
        application = createApplicationAEntity();
        application.setInfo("i");

        MvcResult result = mockMvc.perform(post(APPLICATION_CONTROLLER_REQUEST_URL)
                .accept(MEDIA_TYPE)
                .content(createJsonString(application))
                .contentType(MEDIA_TYPE))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(EMPTY_STRING))
                .andReturn();

        String contentAsString = result.getResponse().getErrorMessage();
        assertTrue(contentAsString.contains(EXCEPTION_NOT_VALID_EXCEPTION_MESSAGE));
        assertTrue(contentAsString.contains("Info should have at least 5 characters"));
    }

    @Test
    public void testSmallNumberValidation() throws Exception {
        application = createApplicationAEntity();
        ((ApplicationA) application).setNumber(0);

        MvcResult result = mockMvc.perform(post(APPLICATION_CONTROLLER_REQUEST_URL)
                .accept(MEDIA_TYPE)
                .content(createJsonString(application))
                .contentType(MEDIA_TYPE))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(EMPTY_STRING))
                .andReturn();

        String contentAsString = result.getResponse().getErrorMessage();
        assertTrue(contentAsString.contains(EXCEPTION_NOT_VALID_EXCEPTION_MESSAGE));
        assertTrue(contentAsString.contains("Number should be more than 1"));
    }

    private String createJsonString(Application application) throws JsonProcessingException {
        String content = mapper.writeValueAsString(application);
        return content.substring(0, content.length() - 1) + ",\"type\":\"aClazz\"}";
    }

}
