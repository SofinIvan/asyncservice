package com.ifree.async.controllers;

import com.ifree.async.enums.ApplicationStatus;
import com.ifree.async.model.Application;
import com.ifree.async.model.ApplicationA;
import com.ifree.async.model.dto.ApplicationDTO;
import com.ifree.async.service.ApplicationService;
import com.ifree.async.service.MessageProducer;
import com.ifree.async.utils.ApplicationConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ifree.async.utils.TestConstants.APPLICATION_CONTROLLER_REQUEST_URL;
import static com.ifree.async.utils.TestConstants.EMPTY_STRING;
import static com.ifree.async.utils.TestConstants.ID;
import static com.ifree.async.utils.TestConstants.JSON_INPUT;
import static com.ifree.async.utils.TestConstants.MEDIA_TYPE;
import static com.ifree.async.utils.TestEntityCreator.createApplicationAEntity;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:spring/dispatcher-servlet.xml")
public class ApplicationControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ApplicationConverter applicationConverter;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private MessageProducer messageProducer;

    private MockMvc mockMvc;

    private ApplicationA applicationA;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        applicationA = createApplicationAEntity();
    }

    @Test
    public void createApplicationCheckResponse() throws Exception {
        given(applicationConverter.fromDTO(Mockito.any(ApplicationDTO.class)))
                .willReturn(applicationA);

        mockMvc.perform(post(APPLICATION_CONTROLLER_REQUEST_URL)
                .accept(MEDIA_TYPE)
                .content(JSON_INPUT)
                .contentType(MEDIA_TYPE))
                .andExpect(status().isAccepted())
                .andExpect(content().string(EMPTY_STRING));
    }


    @Test
    public void createApplicationCheckMethodCalls() throws Exception {
        given(applicationConverter.fromDTO(Mockito.any(ApplicationDTO.class)))
                .willReturn(applicationA);

        mockMvc.perform(post(APPLICATION_CONTROLLER_REQUEST_URL)
                .accept(MEDIA_TYPE)
                .content(JSON_INPUT)
                .contentType(MEDIA_TYPE));

        ArgumentCaptor<Application> applicationArgumentCaptor = ArgumentCaptor.forClass(Application.class);
        ArgumentCaptor<ApplicationDTO> applicationDTOArgumentCaptor = ArgumentCaptor.forClass(ApplicationDTO.class);

        verify(applicationConverter, times(1)).fromDTO(applicationDTOArgumentCaptor.capture());
        verify(applicationService, times(1)).save(applicationArgumentCaptor.capture());
        verify(messageProducer, times(1)).send(applicationArgumentCaptor.capture());

        List<Application> params = applicationArgumentCaptor.getAllValues();
        assertEquals(2, params.size());
        assertEquals(params, new ArrayList<>(Arrays.asList(applicationA, null)));

        List<ApplicationDTO> dtoParams = applicationDTOArgumentCaptor.getAllValues();
        assertEquals(1, dtoParams.size());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void createApplicationCheckMethodOrderCalls() throws Exception {
        given(applicationConverter.fromDTO(Mockito.any(ApplicationDTO.class)))
                .willReturn(applicationA);

        mockMvc.perform(post(APPLICATION_CONTROLLER_REQUEST_URL)
                .accept(MEDIA_TYPE)
                .content(JSON_INPUT)
                .contentType(MEDIA_TYPE));

        InOrder inOrder = inOrder(applicationConverter, applicationService, messageProducer);
        inOrder.verify(applicationConverter).fromDTO(any(ApplicationDTO.class));
        inOrder.verify(applicationService).save(any(Application.class));
        inOrder.verify(messageProducer).send(any(Application.class));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void createApplicationCheckApplicationStatusChanging() throws Exception {
        given(applicationConverter.fromDTO(Mockito.any(ApplicationDTO.class)))
                .willReturn(applicationA);

        mockMvc.perform(post(APPLICATION_CONTROLLER_REQUEST_URL)
                .accept(MEDIA_TYPE)
                .content(JSON_INPUT)
                .contentType(MEDIA_TYPE));

        assertEquals(applicationA.getApplicationStatus(), ApplicationStatus.ACCEPTED);
    }

    @Test
    public void getApplicationCheckResponse() throws Exception {
        applicationA.setApplicationStatus(ApplicationStatus.COMPLETE);
        applicationA.setId(ID);

        given(applicationService.getApplication(ID))
                .willReturn(applicationA);

        String responseBody = "{\"id\":" + ID + ",\"description\":\"a_description\",\"info\":\"a_info\",\"applicationStatus\":\"complete\",\"number\":125}";
        mockMvc.perform(get(APPLICATION_CONTROLLER_REQUEST_URL + "/" + ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MEDIA_TYPE))
                .andExpect(content().string(responseBody));
    }

    @Test
    public void getApplicationCheckMethodCalls() throws Exception {
        applicationA.setApplicationStatus(ApplicationStatus.COMPLETE);
        applicationA.setId(ID);

        given(applicationService.getApplication(ID))
                .willReturn(applicationA);

        mockMvc.perform(get(APPLICATION_CONTROLLER_REQUEST_URL + "/" + ID));

        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        verify(applicationService, times(1)).getApplication(captor.capture());
        List<Long> params = captor.getAllValues();
        assertEquals(1, params.size());
        assertEquals(ID, (long) params.get(0));
    }

    @Configuration
    static class Config {

        @Bean
        public ApplicationConverter applicationConverter() {
            return mock(ApplicationConverter.class);
        }

        @Bean
        public ApplicationService applicationService() {
            return mock(ApplicationService.class);
        }

        @Bean
        public MessageProducer messageProducer() {
            return mock(MessageProducer.class);
        }
    }

}
