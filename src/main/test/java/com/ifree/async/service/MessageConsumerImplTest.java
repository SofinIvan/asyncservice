package com.ifree.async.service;

import com.ifree.async.enums.ApplicationStatus;
import com.ifree.async.model.Application;
import com.ifree.async.model.ApplicationA;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.annotation.DirtiesContext;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageConsumerImplTest {

    private Application application;

    @Mock
    private ApplicationService applicationService;

    @InjectMocks
    private MessageConsumerImpl messageConsumerImpl;

    @Before
    public void beforeTests() throws Exception {
        application = new ApplicationA();
        when(applicationService.save(application)).thenReturn(application);
        doNothing().when(applicationService).process(application);
    }

    @Test
    public void processCheckStatusChanging() throws Exception {
        messageConsumerImpl.process(application);
        assertSame(application.getApplicationStatus(), ApplicationStatus.IN_PROCESS);
    }

    @Test
    public void processCheckMethodCalls() throws Exception {
        ArgumentCaptor<Application> appCapture = ArgumentCaptor.forClass(Application.class);
        messageConsumerImpl.process(application);

        verify(applicationService, times(1)).save(appCapture.capture());
        verify(applicationService, times(1)).process(appCapture.capture());

        assertEquals(new ArrayList<>(Arrays.asList(application, application)), appCapture.getAllValues());
    }

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    public void processCheckMethodOrderCalls() throws Exception {
        messageConsumerImpl.process(application);

        InOrder inOrder = inOrder(applicationService);
        inOrder.verify(applicationService).save(application);
        inOrder.verify(applicationService).process(application);
        inOrder.verifyNoMoreInteractions();
    }

}
