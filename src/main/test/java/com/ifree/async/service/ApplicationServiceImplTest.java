package com.ifree.async.service;

import com.ifree.async.dao.ApplicationARepository;
import com.ifree.async.dao.ApplicationBRepository;
import com.ifree.async.enums.ApplicationStatus;
import com.ifree.async.exceptions.ApplicationNotFoundException;
import com.ifree.async.exceptions.ApplicationNotReadyException;
import com.ifree.async.model.ApplicationA;
import com.ifree.async.model.ApplicationB;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.ifree.async.utils.TestConstants.ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationServiceImplTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private ApplicationARepository aRepository;

    @Mock
    private ApplicationBRepository bRepository;

    @InjectMocks
    private ApplicationServiceImpl appService;

    @Test
    public void getApplicationA() {
        ApplicationA appA = new ApplicationA();
        appA.setApplicationStatus(ApplicationStatus.COMPLETE);
        when(aRepository.findOne(ID)).thenReturn(appA);
        when(bRepository.findOne(ID)).thenReturn(null);

        assertEquals(appA, appService.getApplication(ID));
        verify(aRepository, times(1)).findOne(ID);
        verify(bRepository, times(0)).findOne(ID);
    }

    @Test
    public void getApplicationB() {
        ApplicationB appB = new ApplicationB();
        appB.setApplicationStatus(ApplicationStatus.COMPLETE);
        when(aRepository.findOne(ID)).thenReturn(null);
        when(bRepository.findOne(ID)).thenReturn(appB);

        assertEquals(appB, appService.getApplication(ID));
        verify(aRepository, times(1)).findOne(ID);
        verify(bRepository, times(1)).findOne(ID);
    }

    @Test
    public void getNonexistentApplication() {
        when(aRepository.findOne(ID)).thenReturn(null);
        when(bRepository.findOne(ID)).thenReturn(null);

        exception.expect(ApplicationNotFoundException.class);
        String message = String.format("Application with id '%s' not found", ID);
        exception.expectMessage(message);
        appService.getApplication(ID);
    }

    @Test
    public void getNotReadyApplication() {
        ApplicationB appB = new ApplicationB();
        ApplicationStatus status = ApplicationStatus.FAIL;
        appB.setApplicationStatus(status);
        when(aRepository.findOne(ID)).thenReturn(null);
        when(bRepository.findOne(ID)).thenReturn(appB);

        exception.expect(ApplicationNotReadyException.class);
        String message = String.format("Application with id '%s' not ready, status is: '%s'", ID, status.getCode());
        exception.expectMessage(message);
        appService.getApplication(ID);
    }

    @Test
    public void saveApplicationA() {
        ApplicationA appA = new ApplicationA();
        appA.setId(ID);
        appA.setApplicationStatus(ApplicationStatus.COMPLETE);
        when(aRepository.save(appA)).thenReturn(appA);
        assertEquals(appA, appService.save(appA));
        verify(aRepository, times(1)).save(appA);
        verify(bRepository, times(0)).save(any(ApplicationB.class));
    }

    @Test
    public void saveApplicationB() {
        ApplicationB appB = new ApplicationB();
        appB.setId(ID);
        appB.setApplicationStatus(ApplicationStatus.COMPLETE);
        when(bRepository.save(appB)).thenReturn(appB);
        assertEquals(appB, appService.save(appB));
        verify(aRepository, times(0)).save(any(ApplicationA.class));
        verify(bRepository, times(1)).save(appB);
    }

    @Test
    public void processApplicationA() throws Exception {
        ApplicationA appA = new ApplicationA();
        appA.setId(ID);
        when(aRepository.save(appA)).thenReturn(appA);
        appService.process(appA);

        assertSame(appA.getApplicationStatus(), ApplicationStatus.COMPLETE);
        verify(aRepository, times(1)).save(appA);
        verify(bRepository, times(0)).save(any(ApplicationB.class));
    }

    @Test
    public void processApplicationB() throws Exception {
        ApplicationB appB = new ApplicationB();
        appB.setId(ID);
        when(bRepository.save(appB)).thenReturn(appB);
        appService.process(appB);

        assertSame(appB.getApplicationStatus(), ApplicationStatus.FAIL);
        verify(aRepository, times(0)).save(any(ApplicationA.class));
        verify(bRepository, times(1)).save(appB);
    }

}
