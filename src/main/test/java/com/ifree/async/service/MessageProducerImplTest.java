package com.ifree.async.service;

import com.ifree.async.config.properties.PropertyNames;
import com.ifree.async.model.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.core.env.Environment;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageProducerImplTest {

    @Mock
    private AmqpTemplate amqpTemplate;

    @Mock
    private Environment environment;

    @InjectMocks
    private MessageProducerImpl messageProducerImpl;

    @Test
    public void send() {
        Application mock = mock(Application.class);
        String propertyName = "test_queue";
        when(environment.getProperty(PropertyNames.RABBITMQ_QUEUE_NAME)).thenReturn(propertyName);
        doNothing().when(amqpTemplate).convertAndSend(any(String.class), any(Application.class));

        ArgumentCaptor<Application> appCapture = ArgumentCaptor.forClass(Application.class);
        ArgumentCaptor<String> queueNameCapture = ArgumentCaptor.forClass(String.class);
        messageProducerImpl.send(mock);

        verify(environment, times(1)).getProperty(PropertyNames.RABBITMQ_QUEUE_NAME);
        verify(amqpTemplate, times(1)).convertAndSend(queueNameCapture.capture(), appCapture.capture());

        assertEquals(mock, appCapture.getValue());
        assertEquals(propertyName, queueNameCapture.getValue());
    }

}
