package com.ifree.async.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.http.MediaType;

import java.nio.charset.Charset;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestConstants {

    public static final long ID = 7L;

    public static final MediaType MEDIA_TYPE = new MediaType("application", "json", Charset.forName("UTF-8"));

    public static final String EMPTY_STRING = "";

    public static final String APPLICATION_CONTROLLER_REQUEST_URL = "/v1/applications";

    public static final String JSON_INPUT = "{\n" +
            "\"id\":1,\n" +
            "\"type\":\"aClazz\",\n" +
            "\"description\":\"a_description\",\n" +
            "\"info\":\"a_info\",\n" +
            "\"number\":125\n" +
            "}";

}
