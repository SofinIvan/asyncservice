package com.ifree.async.utils;

import com.ifree.async.model.ApplicationA;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestEntityCreator {

    public static ApplicationA createApplicationAEntity() {
        ApplicationA appA = new ApplicationA();
        appA.setId(1L);
        appA.setDescription("a_description");
        appA.setInfo("a_info");
        appA.setNumber(125);
        return appA;
    }

}
