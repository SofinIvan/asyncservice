package com.ifree.async.utils;

import com.ifree.async.model.Application;
import com.ifree.async.model.dto.ApplicationDTO;

public interface ApplicationConverter {

    Application fromDTO(ApplicationDTO applicationDTO);

}
