package com.ifree.async.utils;

import com.ifree.async.model.Application;
import com.ifree.async.model.ApplicationA;
import com.ifree.async.model.ApplicationB;
import com.ifree.async.model.dto.ApplicationADTO;
import com.ifree.async.model.dto.ApplicationBDTO;
import com.ifree.async.model.dto.ApplicationDTO;
import com.ifree.async.model.dto.ApplicationDTOVisitor;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConverterImpl implements ApplicationConverter {

    @Override
    public Application fromDTO(ApplicationDTO applicationDTO) {

        final Application[] application = new Application[1];

        applicationDTO.accept(new ApplicationDTOVisitor() {
            @Override
            public void visit(ApplicationADTO dto) {
                ApplicationA app = new ApplicationA();
                app.setId(dto.getId());
                app.setDescription(dto.getDescription());
                app.setInfo(dto.getInfo());
                app.setNumber(dto.getNumber());
                application[0] = app;
            }

            @Override
            public void visit(ApplicationBDTO dto) {
                ApplicationB app = new ApplicationB();
                app.setId(dto.getId());
                app.setDescription(dto.getDescription());
                app.setInfo(dto.getInfo());
                app.setFlag(dto.isFlag());
                application[0] = app;
            }
        });
        return application[0];
    }
}
