package com.ifree.async.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    public static final String THREAD_POOL_TASK_EXECUTOR = "threadPoolTaskExecutor";

    public static final String SCHEMA_BACKEND = "backend";

}
