package com.ifree.async.controllers;

import lombok.RequiredArgsConstructor;

import java.util.Date;

@RequiredArgsConstructor
public class ErrorDetails {

    private final Date timestamp;
    private final String message;
    private final String details;

}