package com.ifree.async.controllers;

import com.ifree.async.exceptions.ApplicationNotFoundException;
import com.ifree.async.exceptions.ApplicationNotReadyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@ControllerAdvice(basePackages = {"com.ifree.async.controllers"})
public class ApplicationControllerAdvice {

    private static final String MESSAGE = "Exception with type: '%s', message: '%s'";

    @ExceptionHandler(ApplicationNotFoundException.class)
    void handleApplicationNotFoundException(HttpServletResponse response,
                                            ApplicationNotFoundException ex) throws IOException {
        int status = HttpStatus.NOT_FOUND.value();
        String message = String.format(MESSAGE, ex.getClass().getSimpleName(), ex.getMessage());
        log.error(message);
        response.sendError(status, message);
    }

    @ExceptionHandler(ApplicationNotReadyException.class)
    void handleApplicationNotReadyException(HttpServletResponse response,
                                            ApplicationNotReadyException ex) throws IOException {
        int status = HttpStatus.LOCKED.value();
        String message = String.format(MESSAGE, ex.getClass().getSimpleName(), ex.getMessage());
        log.error(message);
        response.sendError(status, message);
    }

    @ExceptionHandler(Exception.class)
    void handleException(HttpServletResponse response,
                         Exception ex) throws Exception {
        int status = HttpStatus.INTERNAL_SERVER_ERROR.value();
        String message = String.format(MESSAGE, ex.getClass().getSimpleName(), ex.getMessage());
        log.error(message);
        response.sendError(status, message);
    }

}
