package com.ifree.async.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@Slf4j
@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String VALIDATION_LOG_MESSAGE = "Application validation exception '%s";
    private static final String ERROR_DETAIL_MESSAGE = "Application validation failed";

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        String bindingResult = ex.getBindingResult().toString();
        log.error(String.format(VALIDATION_LOG_MESSAGE, bindingResult));
        ErrorDetails errorDetails = new ErrorDetails(new Date(), ERROR_DETAIL_MESSAGE, bindingResult);
        return ResponseEntity.badRequest().body(errorDetails);
    }

}
