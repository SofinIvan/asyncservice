package com.ifree.async.controllers;

import com.ifree.async.enums.ApplicationStatus;
import com.ifree.async.model.Application;
import com.ifree.async.model.dto.ApplicationDTO;
import com.ifree.async.service.ApplicationService;
import com.ifree.async.service.MessageProducer;
import com.ifree.async.utils.ApplicationConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/v1/applications")
@Slf4j
@RequiredArgsConstructor
public class ApplicationController {

    private final ApplicationConverter applicationConverter;
    private final ApplicationService applicationService;
    private final MessageProducer messageProducer;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity createApplication(@Valid @RequestBody ApplicationDTO applicationDTO) {
        log.info("ApplicationController, call createApplication with request body " + applicationDTO);
        Application application = applicationConverter.fromDTO(applicationDTO);

        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        messageProducer.send(applicationService.save(application));

        return ResponseEntity.accepted().build();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getApplication(@PathVariable("id") long id) {
        log.info("ApplicationController, call getApplication with id = " + id);
        return ResponseEntity.ok(applicationService.getApplication(id));
    }

}
