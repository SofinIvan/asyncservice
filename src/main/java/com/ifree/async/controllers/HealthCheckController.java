package com.ifree.async.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {

    private static final String OK = "I'm ok!";

    @GetMapping(value = "/health/check", produces = "text/plain;charset=UTF-8")
    public String checkHealth() {
        return OK;
    }

}
