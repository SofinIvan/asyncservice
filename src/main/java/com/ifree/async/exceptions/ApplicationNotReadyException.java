package com.ifree.async.exceptions;

public class ApplicationNotReadyException extends RuntimeException {

    public ApplicationNotReadyException() {
    }

    public ApplicationNotReadyException(String message) {
        super(message);
    }

}
