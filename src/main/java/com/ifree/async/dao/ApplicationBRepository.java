package com.ifree.async.dao;

import com.ifree.async.model.ApplicationB;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationBRepository extends CrudRepository<ApplicationB, Long> {

}
