package com.ifree.async.dao;

import com.ifree.async.model.ApplicationA;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationARepository extends CrudRepository<ApplicationA, Long> {

}
