package com.ifree.async.service;

import com.ifree.async.config.properties.PropertyNames;
import com.ifree.async.model.Application;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessageProducerImpl implements MessageProducer {

    private final AmqpTemplate template;
    private final Environment environment;

    @Override
    public void send(Application application) {
        log.info("MessageProducerImpl, call send");
        template.convertAndSend(environment.getProperty(PropertyNames.RABBITMQ_QUEUE_NAME), application);
        log.info(String.format("MessageProducerImpl, message with application id = '%s' sent to queue", application.getId()));
    }

}
