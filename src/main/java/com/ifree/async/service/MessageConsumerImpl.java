package com.ifree.async.service;

import com.ifree.async.enums.ApplicationStatus;
import com.ifree.async.model.Application;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static com.ifree.async.utils.Constants.THREAD_POOL_TASK_EXECUTOR;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageConsumerImpl implements MessageConsumer {

    private final ApplicationService applicationService;

    @Async(THREAD_POOL_TASK_EXECUTOR)
    @RabbitListener(queues = "${rabbitmq.queue.name}")
    @Transactional
    public void process(Application application) throws Exception {
        log.info("MessageConsumerImpl, process, received message: " + application.toString());
        application.setApplicationStatus(ApplicationStatus.IN_PROCESS);
        applicationService.save(application);
        try {
            applicationService.process(application);
        } catch (Exception e) {
            log.error("MessageConsumerImpl, exception occurred during application processing", e);
//            because if message will be with error, it makes infinity cycle
//            throw e;
        }
    }

}
