package com.ifree.async.service;

import com.ifree.async.dao.ApplicationARepository;
import com.ifree.async.dao.ApplicationBRepository;
import com.ifree.async.enums.ApplicationStatus;
import com.ifree.async.exceptions.ApplicationNotFoundException;
import com.ifree.async.exceptions.ApplicationNotReadyException;
import com.ifree.async.model.Application;
import com.ifree.async.model.ApplicationA;
import com.ifree.async.model.ApplicationB;
import com.ifree.async.model.ApplicationVisitor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class ApplicationServiceImpl implements ApplicationService {

    private static final String NOT_FOUND = "Application with id '%s' not found";
    private static final String NOT_READY = "Application with id '%s' not ready, status is: '%s'";

    private final ApplicationARepository typeARepository;
    private final ApplicationBRepository typeBRepository;

    @Override
    public void process(Application application) throws Exception {
        log.info("ApplicationServiceImpl, call process");
        Thread.sleep(1000);
        application.accept(new ApplicationVisitor() {
            @Override
            public void visit(ApplicationA application) {
                application.setApplicationStatus(ApplicationStatus.COMPLETE);
                typeARepository.save(application);
            }

            @Override
            public void visit(ApplicationB application) {
                //Just for fun )
                application.setApplicationStatus(ApplicationStatus.FAIL);
                typeBRepository.save(application);
            }
        });
        log.info(String.format("ApplicationServiceImpl, process application with id='%s' and application status='%s' finished", application.getId(), application.getApplicationStatus().getCode()));
    }

    @Override
    public Application save(Application application) {
        log.info("ApplicationServiceImpl, save call");
        final Application[] applicationContainer = new Application[1];
        application.accept(new ApplicationVisitor() {
            @Override
            public void visit(ApplicationA application) {
                applicationContainer[0] = typeARepository.save(application);
            }

            @Override
            public void visit(ApplicationB application) {
                applicationContainer[0] = typeBRepository.save(application);
            }
        });
        log.info(String.format("ApplicationServiceImpl, saved application with id='%s' and application status='%s'", application.getId(), application.getApplicationStatus().getCode()));
        return applicationContainer[0];
    }

    @Override
    @Transactional(readOnly = true)
    public Application getApplication(long id) {
        log.info("ApplicationServiceImpl, getApplication call");
        ApplicationA appA = typeARepository.findOne(id);
        Application app = appA == null ? typeBRepository.findOne(id) : appA;
        if (app == null) {
            String message = String.format(NOT_FOUND, id);
            log.error(message);
            throw new ApplicationNotFoundException(message);
        }
        if (app.getApplicationStatus() != ApplicationStatus.COMPLETE) {
            String message = String.format(NOT_READY, id, app.getApplicationStatus().getCode());
            log.error(message);
            throw new ApplicationNotReadyException(message);
        }
        return app;
    }

}
