package com.ifree.async.service;

import com.ifree.async.model.Application;

public interface MessageProducer {

    void send(Application application);
}
