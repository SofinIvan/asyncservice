package com.ifree.async.service;

import com.ifree.async.model.Application;

public interface ApplicationService {

    void process(Application application) throws Exception;

    Application save(Application application);

    Application getApplication(long id);

}
