package com.ifree.async.service;

import com.ifree.async.model.Application;

public interface MessageConsumer {

    void process(Application application) throws Exception;
}
