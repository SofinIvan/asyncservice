package com.ifree.async.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ApplicationStatus {

    ACCEPTED("accepted"),
    IN_PROCESS("in_process"),
    COMPLETE("complete"),
    FAIL("fail");

    private String code;

    ApplicationStatus(String code) {
        this.code = code;
    }

    @JsonCreator
    public static ApplicationStatus create(String code) {
        for (ApplicationStatus status : values()) {
            if (status.getCode().equalsIgnoreCase(code)) {
                return status;
            }
        }
        return FAIL;
    }

    @JsonValue
    public String getCode() {
        return code;
    }

}
