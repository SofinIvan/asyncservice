package com.ifree.async.model.dto;

public interface ApplicationDTOVisitor {

    void visit(ApplicationADTO application);

    void visit(ApplicationBDTO application);

}
