package com.ifree.async.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Min;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ApplicationADTO extends ApplicationDTO {

    @Min(value = 1, message = "Number should be more than 1")
    private int number;

    @Override
    public void accept(ApplicationDTOVisitor visitor) {
        visitor.visit(this);
    }

}
