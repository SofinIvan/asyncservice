package com.ifree.async.model.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ApplicationADTO.class, name = "aClazz"),
        @JsonSubTypes.Type(value = ApplicationBDTO.class, name = "bClazz")
})
public abstract class ApplicationDTO {

    private long id;

    @Size(min = 3, message = "Description should have at least 3 characters")
    private String description;

    @Size(min = 5, message = "Info should have at least 5 characters")
    private String info;

    public abstract void accept(ApplicationDTOVisitor visitor);

}
