package com.ifree.async.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ApplicationBDTO extends ApplicationDTO {

    private boolean flag;

    @Override
    public void accept(ApplicationDTOVisitor visitor) {
        visitor.visit(this);
    }

}
