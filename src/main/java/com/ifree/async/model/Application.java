package com.ifree.async.model;

import com.ifree.async.enums.ApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;

import static com.ifree.async.utils.Constants.SCHEMA_BACKEND;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Application {

    private static final String SEQUENCE_NAME = "application_id_seq";

    @Id
    @GeneratedValue(generator = SEQUENCE_NAME, strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SCHEMA_BACKEND + "." + SEQUENCE_NAME, allocationSize = 1)
    @Column(name = "id", nullable = false)
    private long id;

    private String description;
    private String info;

    @Column(name = "application_status", columnDefinition = "TEXT")
    @Enumerated(EnumType.STRING)
    private ApplicationStatus applicationStatus;

    public abstract void accept(ApplicationVisitor visitor);

}
