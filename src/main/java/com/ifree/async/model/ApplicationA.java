package com.ifree.async.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "application_type_a")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "id")),
        @AttributeOverride(name = "description", column = @Column(name = "description")),
        @AttributeOverride(name = "info", column = @Column(name = "info")),
        @AttributeOverride(name = "applicationStatus", column = @Column(name = "application_status"))
})
public class ApplicationA extends Application {

    private int number;

    @Override
    public void accept(ApplicationVisitor visitor) {
        visitor.visit(this);
    }

}
