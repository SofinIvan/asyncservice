package com.ifree.async.model;

public interface ApplicationVisitor {

    void visit(ApplicationA application);

    void visit(ApplicationB application);

}
