package com.ifree.async.config.properties;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource({"classpath:rabbit.properties", "classpath:database.properties"})
public class PropertiesConfig {

}
