package com.ifree.async.config.properties;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PropertyNames {

    public static final String DATABASE_URL = "database.url";
    public static final String DATABASE_USERNAME = "database.username";
    public static final String DATABASE_PASSWORD = "database.password";
    public static final String DATABASE_DRIVER = "database.driver.class.name";

    public static final String RABBITMQ_HOST = "rabbitmq.host";
    public static final String RABBITMQ_USERNAME = "rabbitmq.username";
    public static final String RABBITMQ_PASSWORD = "rabbitmq.password";
    public static final String RABBITMQ_QUEUE_NAME = "rabbitmq.queue.name";
    public static final String RABBITMQ_AMOUNT_CONCURRENT_CONSUMERS = "rabbitmq.amount.concurrent.consumers";
    public static final String RABBITMQ_MAX_AMOUNT_CONCURRENT_CONSUMERS = "rabbitmq.max.amount.concurrent.consumers";

}
