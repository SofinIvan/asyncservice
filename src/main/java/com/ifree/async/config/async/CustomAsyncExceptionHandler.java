package com.ifree.async.config.async;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;

@Slf4j
public class CustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

    private static final String MESSAGE_TEMPLATE = "Exception occurred during asynchronous operation, exception: '%s', exception message: '%s', method name: '%s', parameters: '%s'";

    @Override
    public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {
        StringBuilder parameters = new StringBuilder();
        if (obj != null) {
            for (Object o : obj) {
                parameters.append(o);
            }
        }
        log.error(String.format(MESSAGE_TEMPLATE,
                throwable.getClass().getSimpleName(),
                throwable.getMessage(),
                method.getName(),
                parameters), throwable);
    }

}
