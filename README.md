Test task, i-free service
 
Tools:
 JDK 8, Spring 4, JPA 2 / Hibernate 4, JUnit 4, Mockito, Maven 3, Git / Bitbucket, PostgreSQL, IntelliJIDEA
 
To start application: 
1. Specify setting for database (Postgres) in database.properties
2. Execute migrations for database (db/migrations) using command 'mvn flyway:migrate'
3. Specify setting for RabbitMQ in rabbit.properties
4. Build application and start it in servlet container (Tomcat, for example)

**Sofin Ivan**    
September, 2018  