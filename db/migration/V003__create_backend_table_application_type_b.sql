DO $$
DECLARE
  lScriptName    VARCHAR := 'создания таблицы backend.application_type_b';
  lScriptVersion VARCHAR := '3';
  lErrorStack    TEXT;
  lErrorState    TEXT;
  lErrorMsg      TEXT;
  lErrorDetail   TEXT;
  lErrorHint     TEXT;
BEGIN
  BEGIN
    RAISE NOTICE 'Начало % ...', lScriptName;

    -- ****************************************************************************************************************************************
    --     Создание таблицы application_type_a
    -- ****************************************************************************************************************************************
    IF NOT EXISTS(SELECT 1
                  FROM information_schema.columns a
                  WHERE table_name = 'application_type_b'
                    AND table_schema = 'backend')
    THEN
      RAISE NOTICE 'Создание таблицы application_type_b';

      CREATE TABLE backend.application_type_b
      (
        id       BIGINT NOT NULL DEFAULT nextval('backend.application_id_seq' :: REGCLASS),
        description    VARCHAR(255),
        info           VARCHAR(255),
        application_status         TEXT,
        flag           BOOLEAN,
        CONSTRAINT application_type_b_pk PRIMARY KEY (id)
      );

      ALTER TABLE backend.application_type_b
        OWNER TO bug_hunter;
      COMMENT ON TABLE backend.application_type_b
      IS 'Таблица для хранения заявок типа B';

      GRANT ALL ON TABLE backend.application_type_b TO bug_hunter;
      GRANT SELECT, UPDATE, INSERT, DELETE, REFERENCES ON TABLE backend.application_type_b TO bug_hunter_roles;
    END IF;

    EXCEPTION
    WHEN OTHERS
      THEN
        GET STACKED DIAGNOSTICS
        lErrorState = RETURNED_SQLSTATE,
        lErrorMsg = MESSAGE_TEXT,
        lErrorDetail = PG_EXCEPTION_DETAIL,
        lErrorHint = PG_EXCEPTION_HINT,
        lErrorStack = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION ' в скрипте "%" при выполнении кода.
  код       : %
  сообщение : %
  описание  : %
  подсказка : %
  контекст  : %', lScriptVersion, lErrorState, lErrorMsg, lErrorDetail, lErrorHint, lErrorStack;
  END;
END $$;
