DO $$
DECLARE
  lScriptName    VARCHAR := 'создания таблицы backend.application_type_a';
  lScriptVersion VARCHAR := '2';
  lErrorStack    TEXT;
  lErrorState    TEXT;
  lErrorMsg      TEXT;
  lErrorDetail   TEXT;
  lErrorHint     TEXT;
BEGIN
  BEGIN
    RAISE NOTICE 'Начало % ...', lScriptName;

    -- ****************************************************************************************************************************************
    --     Создание таблицы application_type_a
    -- ****************************************************************************************************************************************
    IF NOT EXISTS(SELECT 1
                  FROM information_schema.columns a
                  WHERE table_name = 'application_type_a'
                    AND table_schema = 'backend')
    THEN
      RAISE NOTICE 'Создание таблицы application_type_a';

      CREATE SEQUENCE backend.application_id_seq;
      ALTER TABLE backend.application_id_seq
        OWNER TO bug_hunter;
      GRANT ALL ON SEQUENCE backend.application_id_seq TO bug_hunter, bug_hunter_roles;
      REVOKE ALL ON SEQUENCE backend.application_id_seq FROM bug_hunter;

      CREATE TABLE backend.application_type_a
      (
        id       BIGINT NOT NULL DEFAULT nextval('backend.application_id_seq' :: REGCLASS),
        description    TEXT,
        info           TEXT,
        application_status         TEXT,
        number         INT,
        CONSTRAINT application_type_a_pk PRIMARY KEY (id)
      );

      ALTER TABLE backend.application_type_a
        OWNER TO bug_hunter;
      COMMENT ON TABLE backend.application_type_a
      IS 'Таблица для хранения заявок типа А';

      GRANT ALL ON TABLE backend.application_type_a TO bug_hunter;
      GRANT SELECT, UPDATE, INSERT, DELETE, REFERENCES ON TABLE backend.application_type_a TO bug_hunter_roles;
    END IF;

    EXCEPTION
    WHEN OTHERS
      THEN
        GET STACKED DIAGNOSTICS
        lErrorState = RETURNED_SQLSTATE,
        lErrorMsg = MESSAGE_TEXT,
        lErrorDetail = PG_EXCEPTION_DETAIL,
        lErrorHint = PG_EXCEPTION_HINT,
        lErrorStack = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION ' в скрипте "%" при выполнении кода.
  код       : %
  сообщение : %
  описание  : %
  подсказка : %
  контекст  : %', lScriptVersion, lErrorState, lErrorMsg, lErrorDetail, lErrorHint, lErrorStack;
  END;
END $$;
